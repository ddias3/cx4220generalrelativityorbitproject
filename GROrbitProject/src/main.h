#ifndef MAIN_H
#define MAIN_H

#define GRAVITATIONAL_CONSTANT (6.673e-11)
#define LIGHT_SPEED (299792458.0)

#include <glew.h>
#include <glu.h>
#include <SDL.h>
#include <SDL_opengl.h>
#include <string>

#include "World.h"

//Loads individual image as texture
SDL_Texture* LoadTexture(std::string path, SDL_Renderer **renderer);

//Starts up SDL and creates window
bool Initialize(SDL_GLContext *context, SDL_Window **window, SDL_Renderer **renderer);

bool InitializeGL();

//Shader loading utility programs
void PrintProgramLog(GLuint program);
void PrintShaderLog(GLuint shader);

void Update(World &world);
void Render(World &world);

//Loads media
bool LoadMedia(SDL_Texture **texture, SDL_Renderer **renderer);

//Frees media and shuts down SDL
void Close(SDL_Texture **texture, SDL_Renderer **renderer, SDL_Window **window);

#endif