#ifndef WORLD_H
#define WORLD_H

#include "BlackHole.h"
#include "Particle.h"
#include "Playback.h"

class World
{
private:
	BlackHole blackHole;
	Particle  particle;
	
	Playback playback;

public:
	World();
	
	void Initialize();
	void RunSimulation();
};

#endif