#include "BlackHole.h"
#include "main.h"

BlackHole::BlackHole(Eigen::Vector3d angularMomentum, double mass)
{
	this->angularMomentum = angularMomentum;
	this->mass = mass;
	this->schwarzschildRadius = 2.0 * GRAVITATIONAL_CONSTANT * mass / (LIGHT_SPEED * LIGHT_SPEED);
}
