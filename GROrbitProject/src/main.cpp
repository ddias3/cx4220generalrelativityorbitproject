#include "main.h"
#include <Eigen/Dense>
#include <cstdio>

#include <SDL_image.h>

//Screen dimension constants
const int SCREEN_WIDTH = 640;
const int SCREEN_HEIGHT = 480;

GLuint gProgramID = 0;
GLint gVertexPos2DLocation = -1;
GLuint gVBO = 0;
GLuint gIBO = 0;

int main(int argc, char **argv)
{
	World world;
	
	world.Initialize();

	world.RunSimulation();

	//The window renderer
	SDL_Renderer *renderer = nullptr;

	//Current displayed texture
	SDL_Texture *texture = nullptr;

	//The window we'll be rendering to
	SDL_Window *window = nullptr;

	//OpenGL Context
	SDL_GLContext context;

	//Start up SDL and create window
	if (!Initialize(&context, &window, &renderer))
	{
		printf("Failed to initialize!\n");
	}
	else
	{
		//Load media
		// if (!LoadMedia(&texture, &renderer))
		// {
		// 	printf("Failed to load media!\n");
		// }
		// else
		{	
			//Main loop flag
			bool quit = false;

			//Event handler
			SDL_Event event;

			//Enable text input
			SDL_StartTextInput();

			//While application is running
			while (!quit)
			{
				//Handle events on queue
				while (SDL_PollEvent(&event) != 0)
				{
					//User requests quit
					if (event.type == SDL_QUIT)
					{
						quit = true;
					}
						//Handle keypress with current mouse position
					else if (event.type == SDL_TEXTINPUT)
					{
						int x = 0, y = 0;
						SDL_GetMouseState(&x, &y);
						switch (event.text.text[0])
						{
						case 'w':
							break;
						case 'a':
							break;
						case 's':
							break;
						case 'd':
							break;
						}
					}
				}

				// //Clear screen
				// SDL_RenderClear(renderer);

				// //Render texture to screen
				// SDL_RenderCopy(renderer, texture, nullptr, nullptr);

				// //Update screen
				// SDL_RenderPresent(renderer);

				// Update state of simulation
				Update(world);

				//Render quad
				Render(world);
				
				//Update screen
				SDL_GL_SwapWindow(window);
			}

			//Disable text input
			SDL_StopTextInput();
		}
	}

	//Free resources and close SDL
	Close(&texture, &renderer, &window);

	return 0;
}

void Update(World &world)
{
	//No per frame update needed
}

void Render(World &world)
{
	//Clear color buffer
	glClear(GL_COLOR_BUFFER_BIT);
	
	//Bind program
	glUseProgram(gProgramID);

	//Enable vertex position
	glEnableVertexAttribArray(gVertexPos2DLocation);

	//Set vertex data
	glBindBuffer(GL_ARRAY_BUFFER, gVBO);
	glVertexAttribPointer(gVertexPos2DLocation, 2, GL_FLOAT, GL_FALSE, 2 * sizeof(GLfloat), NULL);

	//Set index data and render
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gIBO);
	glDrawElements(GL_TRIANGLE_FAN, 4, GL_UNSIGNED_INT, NULL);

	//Disable vertex position
	glDisableVertexAttribArray(gVertexPos2DLocation);

	//Unbind program
	glUseProgram((GLuint)0);
}

bool Initialize(SDL_GLContext *context, SDL_Window **window, SDL_Renderer **renderer)
{
	//Initialization flag
	bool success = true;

	//Initialize SDL
	if (SDL_Init(SDL_INIT_VIDEO) < 0)
	{
		printf("SDL could not initialize! SDL_Error: %s\n", SDL_GetError());
		success = false;
	}
	else
	{
		//Use OpenGL 3.1 core
		SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
		SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 0);
		SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_COMPATIBILITY);

		//Set texture filtering to linear
		if (!SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "1"))
		{
			printf("Warning: Linear texture filtering not enabled!\n");
		}

		//Create window
		*window = SDL_CreateWindow("GROrbitProject", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN);

		if (nullptr == *window)
		{
			printf("Window could not be created! SDL_Error: %s\n", SDL_GetError());
			success = false;
		}
		else
		{
			//Create context
			*context = SDL_GL_CreateContext(*window);
			if (nullptr == *context)
			{
				printf("OpenGL context could not be created! SDL Error: %s\n", SDL_GetError());
				success = false;
			}
			else
			{
				//Initialize GLEW
				glewExperimental = GL_TRUE; 
				GLenum glewError = glewInit();
				if (glewError != GLEW_OK)
				{
					printf("Error initializing GLEW! %s\n", glewGetErrorString(glewError));
				}

				//Use Vsync
				if (SDL_GL_SetSwapInterval(1) < 0)
				{
					printf("Warning: Unable to set VSync! SDL Error: %s\n", SDL_GetError());
				}

				//Initialize OpenGL
				if (!InitializeGL())
				{
					printf("Unable to initialize OpenGL!\n");
					success = false;
				}
			}

			//Create renderer for window
			*renderer = SDL_CreateRenderer(*window, -1, SDL_RENDERER_ACCELERATED);

			if (nullptr == *renderer)
			{
				printf("Renderer could not be created! SDL Error: %s\n", SDL_GetError());
				success = false;
			}
			else
			{
				//Initialize renderer color
				SDL_SetRenderDrawColor(*renderer, 0xFF, 0xFF, 0xFF, 0xFF);

				//Initialize PNG loading
				int imgFlags = IMG_INIT_PNG;
				if (!(IMG_Init(imgFlags) & imgFlags))
				{
					printf("SDL_image could not initialize! SDL_image Error: %s\n", IMG_GetError());
					success = false;
				}
			}
		}
	}

	return success;
}

bool InitializeGL()
{
	//Success flag
	bool success = true;

	//Generate program
	gProgramID = glCreateProgram();

	//Create vertex shader
	GLuint vertexShader = glCreateShader(GL_VERTEX_SHADER);

	//Get vertex source
	const GLchar* vertexShaderSource[] =
	{
		"varying vec3 N;\n"
		"varying vec3 v;\n"
		"\n"
		"void main(void)\n"
		"{\n"
		"	v = vec3(gl_ModelViewMatrix * gl_Vertex);\n"
		"	N = normalize(gl_NormalMatrix * gl_Normal);\n"
		"\n"
		"	gl_Position = gl_ModelViewProjectionMatrix * gl_Vertex;\n"
		"}\n"
	};

	//Set vertex source
	glShaderSource(vertexShader, 1, vertexShaderSource, NULL);

	//Compile vertex source
	glCompileShader(vertexShader);

	//Check vertex shader for errors
	GLint vShaderCompiled = GL_FALSE;
	glGetShaderiv(vertexShader, GL_COMPILE_STATUS, &vShaderCompiled);
	if (vShaderCompiled != GL_TRUE)
	{
		printf("Unable to compile vertex shader %d!\n", vertexShader);
		PrintShaderLog(vertexShader);
        return (success = false);
	}

	//Attach vertex shader to program
	glAttachShader(gProgramID, vertexShader);


	//Create fragment shader
	GLuint fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);

	//Get fragment source
	const GLchar* fragmentShaderSource[] =
	{
		varying vec3 N;
		varying vec3 v;

		void main(void)
		{
			vec3 L = normalize(gl_LightSource[0].position.xyz - v);
			vec4 Idiff = gl_FrontLightProduct[0].diffuse * max(dot(N,L), 0.0);
			Idiff = clamp(Idiff, 0.0, 1.0);

			gl_FragColor = Idiff;
		}
	};

	//Set fragment source
	glShaderSource(fragmentShader, 1, fragmentShaderSource, NULL);

	//Compile fragment source
	glCompileShader(fragmentShader);

	//Check fragment shader for errors
	GLint fShaderCompiled = GL_FALSE;
	glGetShaderiv(fragmentShader, GL_COMPILE_STATUS, &fShaderCompiled);
	if (fShaderCompiled != GL_TRUE)
	{
		printf("Unable to compile fragment shader %d!\n", fragmentShader);
		PrintShaderLog(fragmentShader);
		return (success = false);
	}

	//Attach fragment shader to program
	glAttachShader(gProgramID,fragmentShader);

	//Link program
	glLinkProgram(gProgramID);

	//Check for errors
	GLint programSuccess = GL_TRUE;
	glGetProgramiv(gProgramID, GL_LINK_STATUS, &programSuccess);
	if (programSuccess != GL_TRUE)
	{
		printf("Error linking program %d!\n", gProgramID);
		PrintProgramLog(gProgramID);
		return (success = false);
	}
	
	//Get vertex attribute location
	gVertexPos2DLocation = glGetAttribLocation(gProgramID, "LVertexPos2D");
	if (gVertexPos2DLocation == -1)
	{
		printf("LVertexPos2D is not a valid glsl program variable!\n");
		success = false;
	}
	else
	{
		//Initialize clear color
		glClearColor(0.f, 0.f, 0.f, 1.f);

		//VBO data
		GLfloat vertexData[] =
		{
			-0.5f, -0.5f,
			 0.5f, -0.5f,
			 0.5f,  0.5f,
			-0.5f,  0.5f
		};

		//IBO data
		GLuint indexData[] = {0, 1, 2, 3};

		//Create VBO
		glGenBuffers(1, &gVBO);
		glBindBuffer(GL_ARRAY_BUFFER, gVBO);
		glBufferData(GL_ARRAY_BUFFER, 2 * 4 * sizeof(GLfloat), vertexData, GL_STATIC_DRAW);

		//Create IBO
		glGenBuffers(1, &gIBO);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gIBO);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, 4 * sizeof(GLuint), indexData, GL_STATIC_DRAW);
	}

	return success;
}

bool LoadMedia(SDL_Texture **texture, SDL_Renderer **renderer)
{
	//Loading success flag
	bool success = true;

	//Load PNG texture
	*texture = LoadTexture("assets/textures/galaxy.png", renderer);
	if (nullptr == *texture)
	{
		printf("Failed to load texture image!\n");
		success = false;
	}

	return success;
}

void Close(SDL_Texture **texture, SDL_Renderer **renderer, SDL_Window **window)
{
	//Deallocate program
	glDeleteProgram(gProgramID);

	//Free loaded image
	if (nullptr != *texture)
	{
		SDL_DestroyTexture(*texture);
		*texture = nullptr;
	}

	//Destroy window	
	SDL_DestroyRenderer(*renderer);
	SDL_DestroyWindow(*window);
	*window = nullptr;
	*renderer = nullptr;

	//Quit SDL subsystems
	IMG_Quit();
	SDL_Quit();
}

SDL_Texture* LoadTexture(std::string path, SDL_Renderer **renderer)
{
	//The final texture
	SDL_Texture* newTexture = nullptr;

	//Load image at specified path
	SDL_Surface* loadedSurface = IMG_Load(path.c_str());
	if (nullptr == loadedSurface)
	{
		printf("Unable to load image %s! SDL_image Error: %s\n", path.c_str(), IMG_GetError());
	}
	else
	{
		//Create texture from surface pixels
        newTexture = SDL_CreateTextureFromSurface(*renderer, loadedSurface);
		if (nullptr == newTexture)
		{
			printf("Unable to create texture from %s! SDL Error: %s\n", path.c_str(), SDL_GetError());
		}

		//Get rid of old loaded surface
		SDL_FreeSurface(loadedSurface);
	}

	return newTexture;
}

void PrintProgramLog(GLuint program)
{
	//Make sure name is shader
	if(glIsProgram(program))
	{
		//Program log length
		int infoLogLength = 0;
		int maxLength = infoLogLength;
		
		//Get info string length
		glGetProgramiv(program, GL_INFO_LOG_LENGTH, &maxLength);
		
		//Allocate string
		char* infoLog = new char[maxLength];
		
		//Get info log
		glGetProgramInfoLog(program, maxLength, &infoLogLength, infoLog );
		if( infoLogLength > 0 )
		{
			//Print Log
			printf( "%s\n", infoLog );
		}
		
		//Deallocate string
		delete[] infoLog;
	}
	else
	{
		printf( "Name %d is not a program\n", program );
	}
}

void PrintShaderLog( GLuint shader )
{
	//Make sure name is shader
	if( glIsShader( shader ) )
	{
		//Shader log length
		int infoLogLength = 0;
		int maxLength = infoLogLength;
		
		//Get info string length
		glGetShaderiv( shader, GL_INFO_LOG_LENGTH, &maxLength );
		
		//Allocate string
		char* infoLog = new char[ maxLength ];
		
		//Get info log
		glGetShaderInfoLog( shader, maxLength, &infoLogLength, infoLog );
		if( infoLogLength > 0 )
		{
			//Print Log
			printf( "%s\n", infoLog );
		}

		//Deallocate string
		delete[] infoLog;
	}
	else
	{
		printf( "Name %d is not a shader\n", shader );
	}
}