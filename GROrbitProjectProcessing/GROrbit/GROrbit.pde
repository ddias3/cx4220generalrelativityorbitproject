// Program wide variables
PImage skybox;
PShader simplifiedShader;
PShader skyboxShader;
int renderMode = 0;
float fov = PI * 0.3333;
float aspectRatio;

// Control Variables
float cameraTheta = 0;
float cameraHeight = 0;
float cameraDistance = 6000;
PVector cameraLocation;
PMatrix3D cameraRotation;

final float GRAVITATIONAL_CONSTANT = 6.673e-11;
final float LIGHT_SPEED = 299792458.0f;

// Black hole variables
final float X_10_SOLAR_MASSES = 50;
float blackHoleMass = -1.0f;
float blackHoleRadius = -1.0f;

//RayTrace variables
Sphere[] spheres;

final int SCREEN_WIDTH = 800;
final int SCREEN_HEIGHT = 600;

// Initialize variables and load shaders
void setup()
{
  size(SCREEN_WIDTH, SCREEN_HEIGHT, P3D);
  noLights();
  noStroke();
  
  skybox = loadImage("skybox/galaxy.png");
  simplifiedShader = loadShader("shaders/simple.frag", "shaders/simple.vert");
  skyboxShader = loadShader("shaders/skybox.frag", "shaders/skybox.vert");
  
//  String str[] = loadStrings("starting_conditions.txt");
  
//  for (int i = 0; i < str.length; ++i)
//  {    
//    String[] token = splitTokens(str[i], " "); // Get a line and parse tokens.
//    
//    if (token.length == 0)
//    {
//      continue; // Skip blank line.
//    }
//    else if (token[0].equals("BlackHole-Mass"))
//    {
//      blackHoleMass = Float.parseFloat(token[1]) * 10.0f * 1.989e30;
//      blackHoleRadius = (2 * GRAVITATIONAL_CONSTANT * blackHoleMass / (LIGHT_SPEED * LIGHT_SPEED)) * 1.0e-3;
//      println("Black Hole Radius = " + blackHoleRadius);
//    }

  blackHoleMass = X_10_SOLAR_MASSES * 10.0f * 1.989e30;
  blackHoleRadius = (2 * GRAVITATIONAL_CONSTANT * blackHoleMass / (LIGHT_SPEED * LIGHT_SPEED)) * 1.0e-3;
//  println("Black Hole Radius = " + blackHoleRadius);
//  }
  
  spheres = new Sphere[1];
  spheres[0] = new Sphere(blackHoleRadius, 0, 0, 0, 1);

  aspectRatio = (float)SCREEN_WIDTH / (float)SCREEN_HEIGHT;
  perspective(fov, aspectRatio, 0.01, 400000.0);
}

void keyPressed()
{
  if (key == CODED)
  {
    switch (keyCode)
    {
    case LEFT:
      cameraTheta += 6 * 0.1666f;
      if (cameraTheta > 360.0f)
        cameraTheta -= 360.0f;
      break;
    case RIGHT:
      cameraTheta += -6 * 0.1666f;
      if (cameraTheta < 0.0f)
        cameraTheta += 360.0f;
      break;
//    case UP:
//      cameraHeight -= 150 * 0.1666f;
//      if (cameraHeight > 5000.0f)
//        cameraHeight = 5000.0f;
//      break;
//    case DOWN:
//      cameraHeight -= -150 * 0.1666f;
//      if (cameraHeight < -5000.0f)
//        cameraHeight = -5000.0f;
//      break;
    }
  }
  else
  {
    switch (key)
    {
    case ' ':
      renderMode = (renderMode == 0) ? 1 : 0;
      if (renderMode == 1)
        CalculateLensingEffect();
      break;
    }
  }
}

void mouseWheel(MouseEvent event)
{
  if (event.getCount() > 0)
  {
    cameraDistance += -200 * 0.1666f;
    if (cameraDistance < 500.0f)
      cameraDistance = 500.0f;
  }
  else
  {
    cameraDistance += 200 * 0.1666f;
    if (cameraDistance > 20000.0f)
      cameraDistance = 20000.0f;
  }
}

void draw()
{
  if (renderMode == 0)
  {
    resetMatrix();
    
    background(0);
    
    cameraLocation = new PVector(cameraDistance * cos((PI * 0.005555f) * cameraTheta), cameraHeight, cameraDistance * sin((PI * 0.005555f) * cameraTheta));
    camera(cameraLocation.x, cameraLocation.y, cameraLocation.z,
           0, 0, 0,
           0.0, 1.0, 0.0);
        
    // turn to right hand scale with +y being up
    scale(1.0, -1.0, 1.0);

    shader(simplifiedShader);
    drawBlackHole();

    drawSkybox();
  }
}

void drawBlackHole()
{
  pushMatrix();
    scale(blackHoleRadius);
    drawSphere(32, 120, 120, 120);
  popMatrix();
}

void drawParticle()
{
  pushMatrix();
    translate(10000, 0, 0);
    scale(100);
    drawSphere(16, 255, 255, 255);
  popMatrix();
}

void drawSkybox()
{
  pushMatrix();
  
  shader(skyboxShader, TRIANGLE);
  noLights();
  textureMode(NORMAL);
  final int SKYBOX_POLYGONS = 256;
  final float INVERSE_POLYGONS = 1.0f / SKYBOX_POLYGONS;
  final float R = 200000.0f; 
  for (int h = 0; h < SKYBOX_POLYGONS; ++h)
  {
    for (int theta = 0; theta < SKYBOX_POLYGONS; ++theta)
    {
      beginShape(QUADS);
        texture(skybox);
        vertex((R * sin(PI * (h * INVERSE_POLYGONS))) * cos((TWO_PI * INVERSE_POLYGONS) * theta),
               R * ((h * INVERSE_POLYGONS) - (1 - h * INVERSE_POLYGONS)),
               (R * sin(PI * (h * INVERSE_POLYGONS))) * sin((TWO_PI * INVERSE_POLYGONS) * theta),
               theta * INVERSE_POLYGONS, h * INVERSE_POLYGONS);
        vertex((R * sin(PI * (h * INVERSE_POLYGONS))) * cos((TWO_PI * INVERSE_POLYGONS) * (theta + 1)),
               R * ((h * INVERSE_POLYGONS) - (1 - h * INVERSE_POLYGONS)),
               (R * sin(PI * (h * INVERSE_POLYGONS))) * sin((TWO_PI * INVERSE_POLYGONS) * (theta + 1)),
               (theta + 1) * INVERSE_POLYGONS, h * INVERSE_POLYGONS);
        vertex((R * sin(PI * ((h + 1) * INVERSE_POLYGONS))) * cos((TWO_PI * INVERSE_POLYGONS) * (theta + 1)),
               R * (((h + 1) * INVERSE_POLYGONS) - (1 - (h + 1) * INVERSE_POLYGONS)),
               (R * sin(PI * ((h + 1) * INVERSE_POLYGONS))) * sin((TWO_PI * INVERSE_POLYGONS) * (theta + 1)),
               (theta + 1) * INVERSE_POLYGONS, (h + 1) * INVERSE_POLYGONS);
        vertex((R * sin(PI * ((h + 1) * INVERSE_POLYGONS))) * cos((TWO_PI * INVERSE_POLYGONS) * theta),
               R * (((h + 1) * INVERSE_POLYGONS) - (1 - (h + 1) * INVERSE_POLYGONS)),
               (R * sin(PI * ((h + 1) * INVERSE_POLYGONS))) * sin((TWO_PI * INVERSE_POLYGONS) * theta),
               theta * INVERSE_POLYGONS, (h + 1) * INVERSE_POLYGONS);
      endShape();
    }
  }
  popMatrix();
}

void drawSphere(int partitions, float red, float green, float blue)
{
  fill(red, green, blue);
  
  final float INVERSE_POLYGONS = 1.0f / partitions;
  for (int h = partitions - 1; h >= 0; --h)
  {
    float radiusProjected_0 = sqrt(1 - pow((h * INVERSE_POLYGONS) - (1 - h * INVERSE_POLYGONS), 2));
    float radiusProjected_1 = sqrt(1 - pow(((h + 1) * INVERSE_POLYGONS) - (1 - (h + 1) * INVERSE_POLYGONS), 2));
    
    for (int theta = partitions - 1; theta >= 0; --theta)
    {
      beginShape(QUADS);
        vertex(radiusProjected_0 * cos(TWO_PI * INVERSE_POLYGONS * theta),
               (h * INVERSE_POLYGONS) - (1 - h * INVERSE_POLYGONS),
               radiusProjected_0 * sin(TWO_PI * INVERSE_POLYGONS * theta));
               
        vertex(radiusProjected_0 * cos(TWO_PI * INVERSE_POLYGONS * (theta + 1)),
               (h * INVERSE_POLYGONS) - (1 - h * INVERSE_POLYGONS),
               radiusProjected_0 * sin(TWO_PI * INVERSE_POLYGONS * (theta + 1)));
               
        vertex(radiusProjected_1 * cos(TWO_PI * INVERSE_POLYGONS * (theta + 1)),
               ((h + 1) * INVERSE_POLYGONS) - (1 - (h + 1) * INVERSE_POLYGONS),
               radiusProjected_1 * sin(TWO_PI * INVERSE_POLYGONS * (theta + 1)));
               
        vertex(radiusProjected_1 * cos(TWO_PI * INVERSE_POLYGONS * theta),
               ((h + 1) * INVERSE_POLYGONS) - (1 - (h + 1) * INVERSE_POLYGONS),
               radiusProjected_1 * sin(TWO_PI * INVERSE_POLYGONS * theta));
      endShape();
    }
  }
}

void CalculateLensingEffect()
{
  savingLocations = new PVector[5];
  savingDirections = new PVector[5];
  
  float viewPlaneSide = tan(fov * 0.5f);

  PVector rayDirection = new PVector(0, 0, -1);
  
  float pitch = atan2(cameraHeight, cameraDistance);
  cameraRotation = new PMatrix3D(cos((0.0174532925f) * (-cameraTheta + 90)),  sin((0.0174532925f) * (-cameraTheta + 90)) * sin(pitch), sin((0.0174532925f) * (-cameraTheta + 90)) * cos(pitch), 0,
                                 0,                                          cos(pitch),                                             -sin(pitch),                                            0,
                                 -sin((0.0174532925f) * (-cameraTheta + 90)), cos((0.0174532925f) * (-cameraTheta + 90)) * sin(pitch), cos((0.0174532925f) * (-cameraTheta + 90)) * cos(pitch), 0,
                                 0,                                          0,                                                      0,                                                      1);
  
  loadPixels();
  for (int x = 0; x < width; ++x)
  {
    for (int y = 0; y < height; ++y)
    {
      rayDirection.x = (x - width / 2.0f) * 2.0f * (viewPlaneSide * aspectRatio) / width;
      rayDirection.y = -(y - height / 2.0f) * 2.0f * viewPlaneSide / height;
      rayDirection.z = -1;
                            
      PVector rotatedRayDirection = new PVector(0, 0, 0);
      cameraRotation.mult(rayDirection, rotatedRayDirection);
      
//      if (x == 10 && y == 10 || x == 150 && y == 150 || x == 400 && y == 300)
//      {
//        savingLocations[0] = new PVector(cameraLocation.x, cameraLocation.y, cameraLocation.z);
//        savingDirections[0] = new PVector(rotatedRayDirection.x, rotatedRayDirection.y, rotatedRayDirection.z);
//        
//        println("x=" + x + " y=" + y + ", yaw = " + cameraTheta + ", pitch = " + pitch + ", " + savingLocations[0] + " -> " + savingDirections[0] + " (" + rayDirection + ")");
//      }

      Color returnedColor = RayTraceColor(x, y, cameraLocation, rotatedRayDirection, rayDirection, 0.0f, Float.POSITIVE_INFINITY, 0);
      
      pixels[y * width + x] = color(min(returnedColor.r * 255.0f, 255), min(returnedColor.g * 255.0f, 255), min(returnedColor.b * 255.0f, 255));
    }
  }
  updatePixels();
}

PVector[] savingLocations;
PVector[] savingDirections;

void drawLensingRays()
{
  if (null == savingLocations)
    return;
    
  pushMatrix();
  resetMatrix();
    
  resetShader(LINES);
  stroke(255, 255, 0);
  noFill();
  beginShape(LINES);
  for (int n = 0; n < 5; ++n)
  {
    vertex(savingLocations[n].x,
          savingLocations[n].y,
          savingLocations[n].z);
         
    vertex(savingLocations[n].x + savingDirections[n].x * 10000.0f,
           savingLocations[n].y + savingDirections[n].y * 10000.0f,
           savingLocations[n].z + savingDirections[n].z * 10000.0f);
  }
  endShape();
  noStroke();
  
  popMatrix();
}

//void mousePressed()
//{
//  renderMode = 1;
//  DEBUG_X = mouseX;
//  DEBUG_Y = mouseY;
//  CalculateLensingEffect();
//}
