final float RT_EPSILON = 0.000001f;

int DEBUG_X = -1;
int DEBUG_Y = -1;
  
  /**
   * Shoots out a single ray from the point origin in the direction rayDirection and returns
   *  a color, the background color if it doesn't collide with anything.
   *@param origin The point the ray starts from
   *@param rayDirection The direction the ray travels in
   *@param minT The minimum t that counts as a hit
   *@param maxT The maximum t that counts as a hit
   *@param numberRecurision The index of recursion that this ray is, with 0 meaning that it was not recursively shot from a reflection.
   *@return Returns the color that this ray has, where each ray corresponds to a pixel, in the range [0, 1] for the colors r,g,b. 
   */
public Color RayTraceColor(int _x, int _y, PVector rayOrigin, PVector rayDirectionWorld, PVector rayDirectionLocal, float minT, float maxT, int numberRecursion)
{
    // Look for any intersection
  RayTraceIntersection currentIntersection = RayTraceHit(rayOrigin, rayDirectionWorld, minT, maxT);
  Color rayColor = null;
  
    // Calculate Lighting for this intersection
  if (currentIntersection != null)
  {
    if (currentIntersection.id == 1)
      rayColor = new Color(0, 0, 0);
    else
      rayColor = new Color(60, 90, 180);
  }
  else // check for lensing effect
  {
//    rayDirection.normalize();

    if (_x == DEBUG_X && _y == DEBUG_Y) println(" ---- ");
    
    float angleToEdgeOfBlackHole = asin(blackHoleRadius / rayOrigin.mag());
    if (_x == DEBUG_X && _y == DEBUG_Y) println("angleToEdgeOfBlackHole = " + angleToEdgeOfBlackHole + " rads (" + (angleToEdgeOfBlackHole * 180.0f / PI) + " degrees)");
    PVector closestPointOnBlackHole;
    if (rayDirectionLocal.x < 0)
      closestPointOnBlackHole = new PVector(-blackHoleRadius * cos(angleToEdgeOfBlackHole), 0, blackHoleRadius * sin(angleToEdgeOfBlackHole));
    else
      closestPointOnBlackHole = new PVector(blackHoleRadius * cos(angleToEdgeOfBlackHole), 0, blackHoleRadius * sin(angleToEdgeOfBlackHole));
      
    if (_x == DEBUG_X && _y == DEBUG_Y) println("closestPointOnBlackHole = " + closestPointOnBlackHole);
      
    if (_x == DEBUG_X && _y == DEBUG_Y) println("rayDirectionLocal = " + rayDirectionLocal);
      
    float anglePitch = -atan(rayDirectionLocal.y / rayDirectionLocal.x);
//    if (rayDirectionLocal.x < 0)
//      anglePitch = -anglePitch;
      
    if (_x == DEBUG_X && _y == DEBUG_Y) println("anglePitch = " + anglePitch + " rads (" + (anglePitch * 180.0f / PI) + " degrees)");
    PMatrix3D rotatePointPitch = CreateRotationMatrix(new PVector(0, 0, 1), -anglePitch);
    PVector closestPointOnBlackHoleElevated = new PVector();
    rotatePointPitch.mult(closestPointOnBlackHole, closestPointOnBlackHoleElevated);
    if (_x == DEBUG_X && _y == DEBUG_Y) println("closestPointOnBlackHoleElevated = " + closestPointOnBlackHoleElevated);
    if (_x == DEBUG_X && _y == DEBUG_Y) println("closestPointOnBlackHole.mag() = " + closestPointOnBlackHole.mag() + ", closestPointOnBlackHoleElevated.mag() = " + closestPointOnBlackHoleElevated.mag());
    
    PVector closestPointOnBlackHoleWorld = new PVector();
    cameraRotation.mult(closestPointOnBlackHoleElevated, closestPointOnBlackHoleWorld);
    if (_x == DEBUG_X && _y == DEBUG_Y) println("closestPointOnBlackHoleWorld = " + closestPointOnBlackHoleWorld + " mag = " + closestPointOnBlackHoleWorld.mag());
    
    PVector pointAlongRay = new PVector(rayOrigin.x, rayOrigin.y, rayOrigin.z);
    pointAlongRay.add(rayDirectionWorld);
    if (_x == DEBUG_X && _y == DEBUG_Y) println("pointAlongRay = " + pointAlongRay);
    
    PVector temp0 = new PVector(closestPointOnBlackHoleWorld.x, closestPointOnBlackHoleWorld.y, closestPointOnBlackHoleWorld.z);
    temp0.sub(rayOrigin);
    PVector temp1 = new PVector(closestPointOnBlackHoleWorld.x, closestPointOnBlackHoleWorld.y, closestPointOnBlackHoleWorld.z);
    temp1.sub(pointAlongRay);
    PVector numeratorClosestPoint = temp0.cross(temp1);
    if (_x == DEBUG_X && _y == DEBUG_Y) println("numeratorClosestPoint = " + numeratorClosestPoint);
    PVector denominatorClosestPoint = new PVector(pointAlongRay.x - rayOrigin.x, pointAlongRay.y - rayOrigin.y, pointAlongRay.z - rayOrigin.z);
    if (_x == DEBUG_X && _y == DEBUG_Y) println("denominatorClosestPoint = " + denominatorClosestPoint);
    
    float closestApproach = numeratorClosestPoint.mag() / denominatorClosestPoint.mag();
    if (_x == DEBUG_X && _y == DEBUG_Y) println("closestApproach = " + closestApproach);
    
    PVector temp2 = new PVector(rayDirectionWorld.x, rayDirectionWorld.y, rayDirectionWorld.z);
    PVector temp3 = new PVector(rayOrigin.x, rayOrigin.y, rayOrigin.z);
    temp3.sub(closestPointOnBlackHoleWorld);
    float parameterT = -PVector.dot(temp3, rayDirectionWorld) / pow(rayDirectionWorld.mag(), 2);
    temp2.mult(parameterT);
    PVector closestPointToBlackHole = new PVector(rayOrigin.x + temp2.x, rayOrigin.y + temp2.y, rayOrigin.z + temp2.z);
    if (_x == DEBUG_X && _y == DEBUG_Y) println("t = " + parameterT);
    if (_x == DEBUG_X && _y == DEBUG_Y) println("closestPointToBlackHole = " + closestPointToBlackHole);
    
    if (_x == DEBUG_X && _y == DEBUG_Y) println("rayDirectionworld = " + rayDirectionWorld);
    PVector lensingAxis = rayDirectionWorld.cross(closestPointOnBlackHoleWorld);
    if (_x == DEBUG_X && _y == DEBUG_Y) println("lensingAxis = " + lensingAxis);
    lensingAxis.normalize();
    if (_x == DEBUG_X && _y == DEBUG_Y) println("lensingAxis.normalize() = " + lensingAxis);
    
    float lensingAngle = 4 * GRAVITATIONAL_CONSTANT * blackHoleMass / (LIGHT_SPEED * LIGHT_SPEED * closestApproach * 6e3);
    if (_x == DEBUG_X && _y == DEBUG_Y) println("lensingAngle = " + lensingAngle + " rads (" + (lensingAngle * 180.0f / PI) + " degrees)");
    
      // negative because the rotation axis is being by crossing the vectors backwards
    PMatrix3D lensingRotationMatrix = CreateRotationMatrix(lensingAxis, -lensingAngle);
    
    PVector lensingRayDirectionWorld = new PVector();
    lensingRotationMatrix.mult(rayDirectionWorld, lensingRayDirectionWorld);
    if (_x == DEBUG_X && _y == DEBUG_Y) println("lensingRayDirectionWorld = " + lensingRayDirectionWorld);
    
    RayTraceIntersection currentIntersectionAfterLensing = RayTraceHit(closestPointToBlackHole, lensingRayDirectionWorld, 1e8, maxT);
    
    if (currentIntersectionAfterLensing != null)
    {
      rayColor = new Color(60, 90, 180);
    }
    else
    {
      float yaw = atan2(-lensingRayDirectionWorld.z, lensingRayDirectionWorld.x);
      if (yaw < 0)
        yaw += TWO_PI;
      float pitch = lensingRayDirectionWorld.y;
      
      int x = (int)(skybox.width * ((TWO_PI - yaw) / TWO_PI));
      int y = (int)(skybox.height * (pitch + 1) * 0.5f);
      
      int c = skybox.get(x, y);
      rayColor = new Color((c & 0x00FF0000) >> 16, (c & 0x0000FF00) >> 8, (c & 0x000000FF));
    }
    
    if (_x == DEBUG_X && _y == DEBUG_Y)
      println(rayOrigin + " -> " + rayDirectionWorld + " curving to " + closestPointToBlackHole + " -> " + lensingRayDirectionWorld);
      
    if (_x == DEBUG_X && _y == DEBUG_Y) println(" ----- ");
  }
  return rayColor;
}

  /**
   * Determines if a given ray hits a surface by checking against every surface, spheres/triangles, in the
   *  scene, and then returns the closest intersection that is still within the bound (minT, maxT).
   *@param origin The point the ray starts from
   *@param rayDirection The direction the ray travels in
   *@param minT The minimum t that counts as a hit
   *@param maxT The maximum t that counts as a hit
   *@return If there is no hit at all, returns null, otherwise returns an instance of RayTraceIntersection which
   *  carries the point, the value of t, and the surface hit.
   */
public RayTraceIntersection RayTraceHit(PVector origin, PVector rayDirection, float minT, float maxT)
{
  float t = Float.POSITIVE_INFINITY;
  RayTraceIntersection currentIntersection = null;
        
  for (int n = 0; n < spheres.length; ++n)
  {
    RayTraceIntersection rayIntersect = CalculateRayTrace(spheres[n], origin, rayDirection, minT, maxT);
          
    if (rayIntersect != null && rayIntersect.t < (t - RT_EPSILON))
    {
      t = rayIntersect.t;
      currentIntersection = rayIntersect;
    }
  }
  
  return currentIntersection;
}

  /**
   * Determines if a given ray hits a given sphere by plugging in a parametric line equation with an implicit sphere equation. 
   *@param sphere The sphere being tested against.
   *@param origin The point the ray starts from
   *@param direction The direction the ray travels in
   *@param minT The minimum t that counts as a hit
   *@param maxT The maximum t that counts as a hit
   *@return If there is no hit at all, returns null, otherwise returns an instance of RayTraceIntersection which
   *  carries the point, the value of t, and the surface hit.
   */
RayTraceIntersection CalculateRayTrace(final Sphere sphere, final PVector origin, final PVector direction, final float minT, final float maxT)
{
  float a = direction.dot(direction);
  float b = 2 * (direction.dot(origin.x - sphere.center.x, origin.y - sphere.center.y, origin.z - sphere.center.z));
  float c = PVector.dot(new PVector(origin.x - sphere.center.x, origin.y - sphere.center.y, origin.z - sphere.center.z), new PVector(origin.x - sphere.center.x, origin.y - sphere.center.y, origin.z - sphere.center.z)) - sphere.r * sphere.r;
   
  float discriminant = (b * b - 4 * a * c);
  if (discriminant < 0)
  {
    return null;
  }
  else
  {
    float Tp = (-b + sqrt(discriminant)) / (2 * a);
    float Tm = (-b - sqrt(discriminant)) / (2 * a);
    
    float returnT;
    boolean validTm;
    boolean validTp;
    
    if (Tm < minT || Tm > maxT)
      validTm = false;
    else
      validTm = true;
    
    if (Tp < minT || Tp > maxT)
      validTp = false;
    else
      validTp = true;
      
    if (validTm && validTp)
      if (Tm < Tp)
        returnT = Tm;
      else
        returnT = Tp;
    else if (validTm && !validTp)
      returnT = Tm;
    else if (!validTm && validTp)
      returnT = Tp;
    else
      return null;
      
    RayTraceIntersection rayIntersect = new RayTraceIntersection();
    rayIntersect.t = returnT;  
    rayIntersect.id = sphere.id;
    rayIntersect.point = new PVector(origin.x + rayIntersect.t * direction.x,
                                     origin.y + rayIntersect.t * direction.y,
                                     origin.z + rayIntersect.t * direction.z);
                                   
    return rayIntersect;
  }
}

public class Sphere
{
  public PVector center;
  public float r = 1;
  public int id = 0;

  public Sphere(float rr, float xx, float yy, float zz, int i)
  {
    center = new PVector(xx, yy, zz);
    r = rr;
    id = i;
  }
  
  public PVector CalculateNormal(PVector point)
  {
    PVector normal = new PVector((point.x - center.x) / r,
                                 (point.y - center.y) / r,
                                 (point.z - center.z) / r);
    //normal.Normalize();
    return normal;
  }
  
  public String toString()
  {
    return "Sph - r=" + r + ", Location" + center;
  }
}

PMatrix3D CreateRotationMatrix(PVector axis, float angle)
{
  return new PMatrix3D(cos(angle) + axis.x * axis.x * (1 - cos(angle)),          axis.x * axis.y * (1 - cos(angle)) - axis.z * sin(angle), axis.x * axis.z * (1 - cos(angle)) + axis.y * sin(angle), 0,
                       axis.y * axis.x * (1 - cos(angle)) + axis.z * sin(angle), cos(angle) + axis.y * axis.y * (1 - cos(angle)),          axis.y * axis.z * (1 - cos(angle)) - axis.x * sin(angle), 0,
                       axis.z * axis.x * (1 - cos(angle)) - axis.y * sin(angle), axis.z * axis.y * (1 - cos(angle)) + axis.x * sin(angle), cos(angle) + axis.z * axis.z * (1 - cos(angle)),          0,
                       0,                                                        0,                                                        0,                                                        1);
}

public class RayTraceIntersection
{
  public PVector point;
  public float t;
  public int id;
  
  public String toString()
  {
    return "p(t=" + t + ") = " + point;
  }
}

public class Color
{
  public float r;
  public float g;
  public float b;
  
  public Color(int rr, int gg, int bb)
  {
    r = rr / 255.0f;
    g = gg / 255.0f;
    b = bb / 255.0f;
  }
  
  public Color(float rr, float gg, float bb)
  {
    r = rr;
    g = gg;
    b = bb;
  }
  
  public String toString()
  {
    return "[" + r + "," + g + "," + b + "]";
  }
}
