#ifndef BLACK_HOLE_H
#define BLACK_HOLE_H

#include <Eigen/Dense>

class BlackHole
{
private:
	//Eigen::Vector3d position;
	Eigen::Vector3d angularMomentum;
	double mass;
	double schwarzschildRadius;

public:
	BlackHole(Eigen::Vector3d angularMomentum, double mass);
};

#endif