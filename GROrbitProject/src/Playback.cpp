#include "Playback.h"

Snapshot* Playback::GetSnapshot(int index)
{
	if ((unsigned int)index >= snapshots.size() || index < 0)
		return nullptr;

	return snapshots[index];
}