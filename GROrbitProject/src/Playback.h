#ifndef PLAYBACK_H
#define PLAYBACK_H

#include <vector>
#include "Snapshot.h"

class Playback
{
private:
	int numberSnapshots;
	std::vector<Snapshot*> snapshots;

public:
	Snapshot* GetSnapshot(int index);

};

#endif